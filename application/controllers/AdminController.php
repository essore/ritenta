<?php







class AdminController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_adminModel;
	protected $_authService;
	
	protected $_form;	
	
    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');  
		$this->_authService = new Application_Service_Auth();
   	
		$this->_adminModel= new Application_Model_Admin();
		
		$this->view->pazienteForm = $this->getPazienteForm();
		$this->view->faqForm    = $this->getFaqForm();	
		$this->view->updfaqForm = $this->updateFaqForm();
         }
		public function indexAction(){}
		
/******************************************************************************faq******************/

	private function getFaqForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Admin_Faq_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'addFaq'),
					'default',
					true
					));
			return $this->_form;
	}
		
	public function addfaqAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('newfaq');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newfaq');
		}
		$values = $form->getValues();
		$this->_adminModel->saveFaq($values);
		$this->_helper->redirector('faq'); /* rimanda alla action di admin controller*/
 	}
	public function newfaqAction(){ }
		
	public function cancfaqAction(){			
		$riga = $this->_getParam('dom');
		$this->_adminModel->cancellafaq($riga);
		$this->_helper->redirector('faq');
	}
		
	public function faqAction(){
		$test=$this->_adminModel->getFaqById();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	
	
	public function modificafaqAction(){   
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('main');
		}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
				return $this->render('updatefaq');
		}
		
		$domanda= $this->_getParam('dom');//incredibilmente funziona!!
        $id = $this->_adminModel->getFaqById($domanda);
 		$values = $this->_form->getValues();
		
        $data = array(  
                        'domanda' =>$values['domanda'],
                        'risposta' =>$values['risposta'],
                        );
		
		/*$data = array(  
                        'domanda' =>'eros',
                        'risposta' =>'ha modificato la faq con id'.$domanda,
                        );	*/			
						
		//$this->_logger->info('domanda = ');
        $this->_adminModel->updateFaq($data, $domanda);
        $this->_helper->redirector('faq');
	}
	
	

	public function updatefaqAction(){
			}
	
	private function updateFaqForm () {
		$urlHelper = $this->_helper->getHelper('url');
		$this->_form = new Application_Form_Admin_Faq_Add();
		$this->_form->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'modificafaq'
					),
					'default'
					));
		return $this->_form;
	}
		
	
/****paziente*****************************************************************************paziente********/			
	private function getPazienteForm () {
		$urlHelper = $this->_helper->getHelper('url');
		$this->_form = new Application_Form_Admin_Paziente_Add();
		$this->_form->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'addpaziente'),
					'default'
					));
		return $this->_form;
	}
		
	public function addpazienteAction(){

		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('main');
		}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
				return $this->render('newpaziente');
		}
		$values = $form->getValues();
		$this->_adminModel->saveProduct($values);
		$this->_helper->redirector('main');
	}
		

	public function newpazienteAction () {
		
	}
    
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	   	
/**utente********************************************************************************utente*/
		
/*	 public function utenteAction(){
 
		$test=$this->_clinicaModel->getUtenteById();   	
		$this->view->assign(array(
    					'riga' => $test,
    				));
		}

 
 /**clinico*******************************************************************************clinico
	 
	public function clinicoAction(){
		$test=$this->_clinicaModel->getClinico();
		$this->view->assign(array(
 		   				'riga' => $test,
    				));
	}
	
	public function clinicobyidAction(){
		$test=$this->_clinicaModel->getClinicoById();
		$this->view->assign(array(
 		   				'riga' => $test,
    				));
	}*/
	
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}
	
	

	
}
	 