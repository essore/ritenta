    <?php 
    class ClinicoController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_clinicaModel;
	protected $_formpaz;
	protected $form;
	protected $_authService;
	
	
    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');
		$this->_clinicaModel= new Application_Model_Clinico();	 
		$this->_formpaz=  new Application_Form_Clinico_Paziente_Add();	
		$this->view->addpazienteform = $this->getpazForm(); /*link tra vista e form*/
		$this->view->newprofForm = $this->getProfiloClinicoForm();
        $this->_authService = new Application_Service_Auth();

    }
    
	public function indexAction(){}
	
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	   	
		
	/*******************************estrare tutti i clinici********************/	
	public function clinicoAction(){  //la vista di tutti i clinici
	$test=$this->_clinicaModel->getClinico();
	$this->view->assign(array(
	   				'riga' => $test,
					));
	}
	
	public function clinicobyidAction(){//la vista di un clinico
		$id = $this->_getparam('id',null);

		if(!is_null($id)){
			$test=$this->_clinicaModel->getClinicoById($id);
		}else{
			$id = $this->_authService->getIdentity()->idutente;	
			$test=$this->_clinicaModel->getClinicoById($id); //da inserire l'id dell'utente loggato
		}
		$this->view->assign(array(
		   				'clinico' => $test,
					));
	}
	
	public function viewpazienteAction(){//la vista di un paziente
		$id = $this->_getparam('id',null);
		$mio =  $this->_getparam('mio',null);
		
		$test=$this->_clinicaModel->getPazienteById($id);
		$this->view->assign(array(
		   				'paziente' => $test,
		   				'mio'=> $mio
					));
	}
	
	
	
	public function addpazienteAction() {
		
	}
	
	
	public function mypazientiAction(){
    	
		$id=$this->_authService->getIdentity()->idutente;
		$segue=$this->_clinicaModel->mypaziente($id);
    	$nome= array();
		$cognome= array();
		$eta= array();
    	foreach($segue as $x){
    		$pippo = $this->_clinicaModel->getPazienteById($x->idpazi);
			$nome += array($x->idpazi => $pippo->nome);
			$cognome += array($x->idpazi => $pippo->cognome);
			$eta += array($x->idpazi => $pippo->eta);
		}    	
			$this->view->assign(array(
			'segue'=>$segue,
    		'nome' => $nome,
    		'cognome'=> $cognome,
    		'eta'=> $eta
    		)
		);
		
		}
		
	
	private function getpazForm(){
			$urlHelper = $this->_helper->getHelper('url');
			$this->_formpaz = new Application_Form_Clinico_Paziente_Add();
			$this->_formpaz->setAction($urlHelper->url(array(
					'controller' => 'clinico',
					'action' => 'newpaziente'), /*****azione legata alla submit della form*/
					'default',
					true
					));
			return $this->_formpaz;	
	}
	
	public function newpazienteAction(){
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('addpaziente');
			}
		$formpaz=$this->_formpaz;
		if (!$formpaz->isValid($_POST)) {
			return $this->render('addpaziente');
		}
		//con una sola form vogliamo fare l'insert in 2 tabelle distinte, attraverso gli array associativi
		//possiamo separarei i dati e fare l'insert prima nella tabella user poi nella tabella pazienti.
		
		$values = $formpaz->getValues(); //recupero dati dalla form

		$user = array(	'username' =>$values['username'],
						'password' =>$values['password'],
						'ruolo'  =>'paziente'
						);
		$id= $this->_clinicaModel->newutente($user); //insert nella tabella utenti e ritorna 
		$paziente = array(
						'idpaziente' => $id,	
						'nome' =>$values['nome'],
						'cognome' =>$values['cognome'],
						'eta' =>$values['eta'],
						'genere' =>$values['genere'],
						'indirizzo' =>$values['indirizzo'],
						'telefono' =>$values['telefono'],
						'email' =>$values['email'],
						);
		
		$this->_clinicaModel->newpaziente($paziente);
		if($values['associa']){
			
			$info=array('idpazi'=>$id ,
						'idclini'=>$this->_authService->getIdentity()->idutente);
 			$this->_clinicaModel->newsegue($info);
		
		}
		
		
		$parametri = array('id'=> $id);
		$this->_helper->redirector('viewpaziente','clinico',TRUE,$parametri); 
		 /* rimanda alla visualizzazione del paziente*/
		/*'clinicobyid/id/'.$id -> stampa: http://localhost/ritenta/public/index.php/clinico/clinicobyid%2Fid%2F35*/
	}
	
/****cucx********************************************/
	public function newprofAction(){
        
    }	
	private function getProfiloClinicoForm(){
       $urlHelper = $this->_helper->getHelper('url');
       $this->_form = new Application_Form_Clinico_Modprof();
        $this->_form->setAction($urlHelper->url(array(
                'controller' => 'clinico',
                'action' => 'updateclinico'),
                'default'
        ));
       return $this->_form;
   }
	
	
	public function updateclinicoAction()
    {
        if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('clinicobyid');
        }
        $form=$this->_form;
        if (!$form->isValid($_POST)) {
            return $this->render('newprof');
        }
		$var= $this->_authService->getIdentity()->idutente;
        //$id = $this->_clinicaModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                        'cognome' =>$values['cognome'],
                        'ruolo' =>$values['ruolo'],
                        'specializzazione' =>$values['specializzazione'],
                        );
        $this->_clinicaModel->updateClinico($data, $var);
        $this->_helper->redirector('clinicobyid');
    }
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}

  	public function faqAction(){
    	
		$test=$this->_clinicoModel->getFaq();
    	
			$this->view->assign(array(
    		'riga' => $test,
    		)
		);
		
		}  	
	
	
}	