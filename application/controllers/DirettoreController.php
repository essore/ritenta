    <?php 
    class DirettoreController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_direttoreModel;
	protected $_form;
	protected $_authService;
	protected $_clinicaModel;
	
    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');  
		$this->_direttoreModel= new Application_Model_Direttore();		
		$this->view->disturboForm = $this->getDisturboForm();
        $this->_authService = new Application_Service_Auth();
		//$this->_clinicaModel= new Application_Model_Clinico();
		
    }
	
	public function indexAction(){}

    
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	   	
	public function pazientiAction(){  //la vista di tutti i pazienti
	$test=$this->_direttoreModel->getpazienti();
	$this->view->assign(array(
	   				'riga' => $test,
					));
	}



/**************************disturbi*********/
public function newdisturboAction(){
		
	}
	
	public function disturboAction(){
		$test=$this->_direttoreModel->getDisturbo();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	
	public function adddisturboAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('disturbo');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newdisturbo');
		}
		$values = $form->getValues();
		$this->_direttoreModel->saveDisturbo($values);
		$this->_helper->redirector('disturbo'); /* rimanda alla action di admin controller*/
 	}	   
		
		private function getDisturboForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Direttore_Disturbo_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'addDisturbo'),
					'default',
					true
					));
			return $this->_form;
	}   
		public function cancelladisturboAction(){			
		$riga = $this->_getParam('disturbo');
		$this->_direttoreModel->cancellaDisturbo($riga);
		$this->_helper->redirector('disturbo');
	}
/******************************************clinico************************/
public function clinicoAction(){
		$test=$this->_direttoreModel->getClinico();
		$this->view->assign(array(
 		   				'riga' => $test,
    				));
	}
	
	/*public function clinicobyidAction(){
		$test=$this->_clinicaModel->getClinicoById();
		$this->view->assign(array(
 		   				'riga' => $test,
    				));
	}
/************************************************logout*******************/

		public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}

}
