    <?php 
    class PazienteController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_pazienteModel;
	protected $_form;
	protected $_authService;
	
    public function init()
    {
		$this->_helper->layout->setLayout('paziente');
		$this->_logger = Zend_Registry::get('log');
		$this -> _pazienteModel = new Application_Model_Paziente();	
		$this->view->episodioForm = $this->getEpisodioForm(); 
	//	$this -> _guestModel = new Application_Model_Guest();	
        $this->_authService = new Application_Service_Auth();	
    }
    
	public function indexAction(){}

	
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	
	public function profiloAction() {
		$mioid =$this->_authService->getIdentity()->idutente;
		$profilo = $this->_pazienteModel->getPaziente($mioid);
		$this -> view -> assign (array(
									'profilo' => $profilo,
									 )); 
	}
	
	public function newepisodioAction(){
		
	}
	
	public function episodioAction(){
		$mioid=$this->_authService->getIdentity()->idutente;
		$test=$this->_pazienteModel->getEpisodio($mioid);//estrae tutti gli episodi
		$app=array();//array vuoto
		foreach($test as $x){					//scorro tutti gli episodi
			$num=$x->disturbo;					//estrai l'id del disturdo presente nella tabella episodi
			$var=$this->_pazienteModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
			$app+=array($num=>$var->nome);		//metto in coda all'array associativo feature id valore nome disturbo
		}
		
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'riga' => $test,
			   		'dist'=>$app,
    				));
		
		}
	
	public function addepisodioAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('episodio');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newepisodio');
		}
		$values = $form->getValues();
		$mioid =$this->_authService->getIdentity()->idutente;
		$values += array('paziente'=> $mioid);
		$this->_pazienteModel->saveEpisodio($values);
		$this->_helper->redirector('episodio'); /* rimanda alla action di admin controller*/
 	}	   
		
	private function getEpisodioForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Paziente_Episodio_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'paziente',
					'action' => 'addEpisodio'),
					'default',
					true
					));
			return $this->_form;
	}   
	public function cancellaepisodioAction(){			
		$riga = $this->_getParam('id');
		$this->_pazienteModel->cancellaEpisodio($riga);
		$this->_helper->redirector('episodio');
	}
		
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}
	
  	public function faqAction(){
    	
		$test=$this->_pazienteModel->getFaq();
    	
			$this->view->assign(array(
    		'riga' => $test,
    		)
		);
		
		}  	
}
