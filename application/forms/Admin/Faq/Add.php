<?php
class Application_Form_Admin_Faq_Add extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addfaq');
		$this->setAction('');

		$this->addElement('text', 'domanda', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'domanda',
            ));
			
			$this->addElement('text', 'risposta', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'risposta',
            ));
        

        $this->addElement('submit', 'Faq', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }
}
	