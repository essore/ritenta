<?php
class Application_Form_Admin_Faq_Upd extends App_Form_Abstract
{
//	protected $_adminModel;
//al momento non è usata... cambiare metodo  updateFaqForm	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('updatefaq');
		$this->setAction('');
		
		/*capire come inviare parametro verso form
		$_adminModel=new Application_Model_Admin();
		
		
		$domanda= $this->_getParam('dom');//incredibilmente funziona!!
        $id = $_adminModel->getFaqById($domanda);
*/
		$this->addElement('text', 'domanda', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'domanda',
            ));
			
			$this->addElement('text', 'risposta', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'risposta',
            ));
        

        $this->addElement('submit', 'Faq', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }
}