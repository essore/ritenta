<?php
class Application_Form_Admin_Paziente_Add extends App_Form_Abstract
{

	public function init()
	{
		$this->setMethod('post');
		$this->setName('addpaziente');
		$this->setAction('');

$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'nome',
            ));
        
        $this->addElement('text', 'passwd', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'cognome',
            ));

        $this->addElement('submit', 'login', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }

}