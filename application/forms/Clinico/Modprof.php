<?php
class Application_Form_Clinico_Modprof extends App_Form_Abstract
{
	
	protected $_clinicoModel;
	protected $_authService;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('modprof');
		$this->setAction('');
		$this->_clinicoModel = new Application_Model_Clinico();
        $this->_authService = new Application_Service_Auth();
		

		$id = $this->_authService->getIdentity()->idutente;
		$var = $this->_clinicoModel->getClinicoById($id);
		
		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->nome,
            'label'      => 'nome',
            ));
			
			$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->cognome,
            'label'      => 'cognome',
            ));
			
			$this->addElement('select', 'ruolo',array(
			'multiOptions'=>array('fisioterapista'=>'fisioterapista', 'medico' => 'medico'),
            'required'   => true,
            'value'      => $var->ruolo,
            'label'      => 'ruolo',
            ));
			
			$this->addElement('text', 'specializzazione', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->specializzazione,
            'label'      => 'specializzazione',
            ));

        $this->addElement('submit', 'Aggiorna', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }
}
	