<?php
class Application_Form_Clinico_Paziente_Add extends Zend_Form
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addpaziente');
		$this->setAction('');

		$this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'username',
            ));
			
			$this->addElement('text', 'password', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'		 => 'pass',
            'label'      => 'password',
            ));
		/*	
        $this->addElement('select', 'livello', array(
            'label' => 'Categoria',
            'required' => true,
        	'multiOptions' => array('1'),
        ));*/
        /****************per ora specifica solo l'inserimento di un nuovo utente**/
        
        $this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Nome',
            ));
		$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Cognome',
            ));
			
		$this->addElement('text', 'eta', array(
            'validators' => array( array('int',TRUE)),
            'required'   => true,
            'label'      => 'Eta',
        ));
		
		$this->addElement('radio', 'genere', array(
		    'label'=>'Sesso',
		    'multiOptions'=>array(
		        'maschio' => 'maschio',
		        'femmina' => 'femmina',
		        'misto' => 'poco',
		    ),
		));
		
		$this->addElement('text', 'indirizzo', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => FALSE,
            'label'      => 'Indirizzo',
        ));
		
		$this->addElement('text', 'telefono', array(
            'validators' => array( array('int',TRUE)),
            'required'   => FALSE,
            'label'      => 'telefono',
        ));

		$this->addElement('text', 'email', array(
		    'filters'    => array('StringTrim', 'StringToLower'),
		    'validators' => array(
		        array('StringLength', TRUE, array(3, 32))
		    ),
		    'required'   => FALSE,
		    'label'      => 'email',
		));
		
		$this->addElement('radio', 'associa', array(
		    'label'=>'E\' un tuo paziente?',
		    'multiOptions'=>array(
		        TRUE => 'si',
		        FALSE  => 'no',
		    ),
		    'value' => TRUE,
		));

        $this->addElement('submit', 'paziente', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }
}
	