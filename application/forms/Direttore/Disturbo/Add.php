<?php
class Application_Form_Direttore_Disturbo_Add extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addditurbo');
		$this->setAction('');

		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'nome disturbo',
            ));
			
        

        $this->addElement('submit', 'Disturbo', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    }
	
	
	
	
}
	