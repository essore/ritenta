<?php
class Application_Form_Paziente_Episodio_Add extends Zend_Form
{
	protected $_pazienteModel;
	protected $_authService;
	protected $_logger;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addepisodio');
		$this->setAction('');
		$_pazienteModel= new Application_Model_Paziente;
		$this->_authService = new Application_Service_Auth();	
		$this->_logger = Zend_Registry::get('log');  	
		
		
		
		$mioid = $this->_authService->getIdentity()->idutente;
		$mieidisturbi = $_pazienteModel->getDisturbiDi($mioid);	
		$opzioni=array();
		$this->_logger->info('eros - Valore di $mioid'. $mioid);
		
		foreach($mieidisturbi as $x){					
			$num=$x->iddisturbo;					
			$dolore = $_pazienteModel->getdisturbobyid($num);
			$opzioni+=array($num => $dolore->nome);		
		}
		$this->addElement('select', 'disturbo', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => true,
   		    'multiOptions' => $opzioni,
     		'label'      => 'disturbo',
        ));

/*		$this->addElement('text', 'data', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array('Int'),
            'required'   => true,
            'label'      => 'data',
            ));*/
            
            
            
  		$this->addElement('text', 'durata', array(
            'validators' => array('Int'),
            'required'   => true,
            'label'      => 'durata in secondi',
            ));
			
			
		$this->addElement('select', 'intensita', array(
 			'multiOptions' => array('1','2','3','4','5','6','7','8','9','10'),
            'required'   => true,
            'label'      => 'intensita',
            ));
			

		

        

         $this->addElement('submit', 'Episodio', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            ));
    
    }
	
	

	
}