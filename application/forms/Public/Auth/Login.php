<?php

class Application_Form_Public_Auth_Login extends App_Form_Abstract
{
	public function init()
    {               
        $this->setMethod('post');
        $this->setName('login');
        $this->setAction('');
    	
        $this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'Username',
            ));
        
        $this->addElement('password', 'password', array(
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'Password',
            ));

        $this->addElement('submit', 'login', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'Login',
            ));
    }
}
