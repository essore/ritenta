<?php 

class Application_Model_Acl extends Zend_Acl
{
	public function __construct()
	{
		// ACL for default role
		$this->addRole(new Zend_Acl_Role('unregistered'))
			 ->add(new Zend_Acl_Resource('public'))
			 ->add(new Zend_Acl_Resource('error'))
			 ->add(new Zend_Acl_Resource('index'))
			 ->allow('unregistered', array('public','error','index'));
			 
		// ACL for user
		$this->addRole(new Zend_Acl_Role('paziente'), 'public')
			 ->add(new Zend_Acl_Resource('paziente'))
			 ->allow('paziente','paziente');
				   
		// ACL for administrator
		$this->addRole(new Zend_Acl_Role('clinico'), 'public')
			 ->add(new Zend_Acl_Resource('clinico'))
			 ->allow('clinico','clinico');
			 
		$this->addRole(new Zend_Acl_Role('direttore'), 'clinico')
			 ->add(new Zend_Acl_Resource('direttore'))
			 ->allow('direttore','direttore');
			 
		$this->addRole(new Zend_Acl_Role('admin'), 'direttore')
			 ->add(new Zend_Acl_Resource('admin'))
			 ->allow('admin','admin');
	}
}