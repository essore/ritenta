<?php

class Application_Model_Admin extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
	public function saveFaq($info)
    {
        return $this->getResource('Faq')->insertFaq($info);
    }
	
	public function cancellafaq($id)
	{
		return $this->getResource('Faq')->cancellaFaq($id);
		
	}
	 public function getFaqById() 
    {
        return $this->getResource('Faq')->getFaq();
    }
    public function getUtenteByName($info)
    {
    	return $this->getResource('Utente')->getUtenteByName($info);
    }
	
	public function updateFaq($data, $id){ 
        return $this->getResource('Faq')->updateFaq($data, $id);
    }
	
}