<?php

class Application_Model_Clinico extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	/* public function getFaqById()   ***************da cambiare*
    {
        return $this->getResource('Faq')->getFaq();
    }
	
	
	
	
	 public function getUtenteById()
    {
        return $this->getResource('Utente')->getUtenteById();
    }
	*/
	
	
	public function getClinico()
    {
        return $this->getResource('Clinico')->getClinico();
    }
	
	public function getClinicoById($id)
	{
		return $this->getResource('Clinico')->getClinicoById($id);
	}
	
	public function newutente($info)
	{
		return $this->getResource('Utente')->newutente($info);
	}
	public function newpaziente($info)
	{
		return $this->getResource('Paziente')->newpaziente($info);
	}
	public function getPazienteById($id)
	{
		return $this->getResource('Paziente')->getPaziente($id);
	}
	
	
	    public function getFaq() 
    {
        return $this->getResource('Faq')->getFaq();
    }
	
	public function updateClinico($var, $id){ // funzione che richiama /resources/Clinico
        return $this->getResource('Clinico')->updateClinico($var, $id);
    }
	
	public function mypaziente($var){
		return $this->getResource('Segue')->mypaziente($var);
	}
}
