<?php

class Application_Model_Direttore extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
    public function getpazienti() /* da eliminare e usare guest sul public controller*/
    {
        return $this->getResource('Paziente')->getpazienti();
    }
	
	public function saveDisturbo($pippo){ //funzione anche del direttore
		return $this->getResource('Disturbo')->insertDisturbo($pippo);
	}
	
	public function getDisturbo()   
    {
        return $this->getResource('Disturbo')->getDisturbo();
		
    }
	
	public function cancellaDisturbo($iddisturbo)
	{
		return $this->getResource('Disturbo')->cancellaDisturbo($iddisturbo);
		
	}	
	public function get($id)
	{
		return $this->getResource('Segue')->get($id);
	}
	public function getClinico() /* da eliminare e usare guest sul public controller*/
    {
        return $this->getResource('Clinico')->getClinico();
    }
}