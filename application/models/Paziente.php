<?php

class Application_Model_Paziente extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
    public function getPaziente($var)
    {
        return $this->getResource('Paziente')->getPaziente($var);
    }
	public function saveEpisodio($pippo){ //funzione anche del direttore
		return $this->getResource('Episodio')->insertEpisodio($pippo);
	}
	
	public function getEpisodio()   
    {
        return $this->getResource('Episodio')->getEpisodio();
		
    }
	
	public function cancellaEpisodio($idepisodio)
	{
		
		return $this->getResource('Episodio')->cancellaEpisodio($idepisodio);
		
	}	
	/*public function getDisturbo()
	{
		
		return $this->getResource('Disturbo')->getDisturbo();
		
	}	*/
	public function getdisturbobyid($var)
	{
		
		return $this->getResource('Disturbo')->getdisturbobyid($var);
	}	
	
	public function getDisturbiDi($var)
	{
		
		return $this->getResource('Patologia')->getDisturbiDi($var);
	}	
	
}
	
	