<?php

class Application_Resource_Clinico extends Zend_Db_Table_Abstract {
	protected $_name = 'clinico';
	protected $_primary = 'idclinico';
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	// Estrae i dati della categoria $idclinico
	public function getClinico() {
		$select = $this -> select();
		return $this -> fetchAll($select);
	}
public function getClinicoById($id) {
		return $this->find($id)->current();
		
	}
public function updateClinico($var, $id) {
        $table = 'clinico'; //nome tabella
        $where = 'idclinico='.$id;
        $this->update($var, $where, $table);
    }
	
}
