<?php

class Application_Resource_Episodio extends Zend_Db_Table_Abstract {
	protected $_name    = 'episodio';
    protected $_primary  = 'idepisodio';
   	//protected $_rowClass = 'Application_Resource_Disturbo_Item';
	
	public function init(){}
	
	public function insertEpisodio($pippo){
		$this->insert($pippo);
	}
	public function getEpisodio()
    {
       $select= $this->select();
	   return $this->fetchAll($select);
    }
	public function cancellaEpisodio($idepisodio){
		$where = "idepisodio=".$idepisodio;
		$this->delete($where);
	}
}