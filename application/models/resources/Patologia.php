<?php

class Application_Resource_Patologia extends Zend_Db_Table_Abstract {
	protected $_name = 'patologia';
	protected $_primary = array('idpaziente','iddisturbo');

	public function getDisturbiDi($var){
		$where = 'idpaziente='.$var;
		$sel= $this->select()
			 ->where($where);
		return $this->fetchAll($sel);
	}
}
?>