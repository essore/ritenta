<?php

class Application_Resource_Paziente extends Zend_Db_Table_Abstract {
	protected $_name    = 'paziente';
    protected $_primary  = 'idpaziente';
  //  protected $_rowClass = 'Application_Resource_Utente_Item';
    
	public function init()
    {
    }
	
	// Estrae i dati della categoria $idutente
    public function getPaziente($id)
    {
	   return $this -> find($id) -> current();
    }
	public function getpazienti() {
		$select = $this->select();
       return $this->fetchAll($select);
	}
	public function newpaziente($info){
		$this->insert($info);
	}
	
}
