<?php

class Application_Resource_Segue extends Zend_Db_Table_Abstract {
	protected $_name = 'segue';
	protected $_primary = array('idpazi','idclini');
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	public function newsegue($info) {
		$this->insert($info);
	}
	
	public function get($id) {
		return $this->find($id)->current();	
	}
	
}
